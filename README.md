## Google Kubernetes Service (GKS) Utilities

This set of simple shell scripts is meant to capture the command lines that yield the necessary information to set up
a Google cluster to be used with GitLab.

The scripts assume that you have already configured your system you are running them from using the gcloud and kubectl
CLI. You can find more information on how
to [setup the gcloud and kubectl CLI](https://cloud.google.com/kubernetes-engine/docs/quickstart#before-you-begin) from 
the Google documentation pages.

### Usage

#### get-token.sh
`get-token.sh` requires that you first run `set-svc-acct.sh` to set a priveleged service account with the permissions to
edit the cluster (needed for installing apps into it). The script will then get the token for this service account. In order to
have these permissions, the service account gets bound to a cluster-admin role. This means that in order to run this script,
you need to be using an account that has `clusterrolebinding` permissions or be assigned the `Kubernetes Engine Admin` role in
the GCP project this cluster resides in.

`get-token.sh` now requires you to specify which service account to get the token from, using the "-t" switch. If you setup the
service account using the `set-svc-acct.sh` script then putting "gitlab*" should find it.

For example:
```bash
% ./get-token.sh -c cluster-foo -t gitlab*
```

#### clean-cluster.sh

`clean-cluster.sh` is intended to be used for cleaning up a kubernetes cluster after its       
integration is removed from GitLab. This will allow you to reinstall software 
via GitLab cleanly. Currently, it ensures Tiller, Ingress, Prometheus, and       
Runner are cleaned up after.    

#### get-dns.sh
`get-dns.sh` is usually the last of the utilities you'll run. It just queries for the Ingress public IP address. It has two small advantages over getting it from the GitLab UI. 1) if you are impatient of in a rush, you can run it again and again on the CLI until the IP is found, often times faster than the web UI shows it, 2) it prints out the found IP in the fowm of a nip.io domain so that you can copy-n-paste right into the domain form field.

#### get-cluster-info.sh
`get-cluster-info.sh` just runs all the other scripts in order of the GitLab Kubernetes configuration screen. One script to run to get you all the info you should need.

### Todo
- Finish the delete cluster script based off of the information at [https://cloud.google.com/kubernetes-engine/docs/quickstart#clean-up](https://cloud.google.com/kubernetes-engine/docs/quickstart#clean-up).

