#!/bin/bash

SVC_ACCT_FILE="/tmp/gitlab-service-account.yaml"
CLST_BND_FILE="/tmp/gitlab-cluster-role-binding.yaml"

cat > $SVC_ACCT_FILE <<EOF-1
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
EOF-1

cat > $CLST_BND_FILE <<EOF-2
 kind: ClusterRoleBinding
 apiVersion: rbac.authorization.k8s.io/v1
 metadata:
   name: gitlab-cluster-admin
 subjects:
 - kind: ServiceAccount
   name: gitlab
   namespace: kube-system
 roleRef:
   kind: ClusterRole
   name: cluster-admin
   apiGroup: rbac.authorization.k8s.io
EOF-2

if [ $# -lt 1 ]; then
   echo "Error: cluster name required."
   echo "Usage: $(basename $0) <cluster name>"
   exit 1
else
   CLUSTER="$1"
fi

CLUSTER_FULL="`kubectl config get-contexts | grep "$CLUSTER" | awk '{if ($1 != "*") {print $2} else {print $3}}'`"

# Apply the service account to your cluster
kubectl --cluster="$CLUSTER_FULL" apply -f $SVC_ACCT_FILE 
rm -f $SVC_ACCT_FILE

# Apply the cluster role binding to your cluster
kubectl --cluster="$CLUSTER_FULL" apply -f $CLST_BND_FILE
rm -f $CLST_BND_FILE

