#!/bin/bash

if [ $# -lt 1 ]; then
   echo "Error: cluster name required."
   echo "Usage: $(basename $0) <cluster name> [region]"
   exit 1
else
   CLUSTER="$1"
   if [ -n "$2" ]; then
      REGION="$2"
   else
      REGION="`aws configure list | awk '/region/{print $2}'`"
   fi
fi

echo "---------------------------------"
./get-url.sh $CLUSTER
echo "---------------------------------"
./get-cert.sh
echo "---------------------------------"
./set-svc-acct.sh $CLUSTER
./get-token.sh -c $CLUSTER -t gitlab*
echo "---------------------------------"
./get-dns.sh $CLUSTER
