#!/bin/bash

if [ "$#" -lt 1 ]; then
   echo "Usage: `basename $0` <cluster name>"
   exit 1
else
   CLUSTER="$1"
fi

CONTEXT="`kubectl config get-contexts | grep "$CLUSTER" | awk '{if ($1 != "*") {print $1} else {print $2}}'`"
INGRESS_IP="`kubectl --context="$CONTEXT" get service ingress-nginx-ingress-controller -n gitlab-managed-apps -o jsonpath="{.status.loadBalancer.ingress[0].ip}"`"

echo ""
echo "CONTEXT:        $CONTEXT"
echo "INGRESS IP:     $INGRESS_IP"
echo ""
echo "Domainname:     ${INGRESS_IP}.nip.io"
echo ""
